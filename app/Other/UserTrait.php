<?php
namespace App\Traits;


trait User
{
    
    /**
     * changeCommand
     *  
     * @param  string $commandName 
     * имя команды, которую нужно сделать активной и отправить пользователю
     * @param  boolean $deep  
     * жесткое ли присваивание. Если нет, - выполнится проврека на присутствие команды в текущей зоне видимости 
     *
     * @return boolean
     * Удалось или не удалось сменить команду
     */
    public function changeCommand($newCommand,$deep = 0)
    {
        //если передана название команды, получаем ее instance
        if(gettype($newCommand) == 'string')
            $newCommand = $this->getCommandInstance($newCommand);

        $allowChanging = 0;
        if($deep == 1)
        {
            $allowChanging = 1;
        } 
        //проверяем чтобы команда на которую нужно переключиться была в зоне видимости текущей команды
        elseif($this->commandInVisibility($newCommand))
        {
            //Если это повтор команды, проверяем чтобы текущую команду можно было повторять
            //также проверяем чтобы message id отличался от всех, на которые мы отвечали ранее
            if(!($newCommand->canRepeat == 0 && $this->currentCommand == $newCommand->name) && 
                 $this->isNotRepeat($this->input->message->id))
            {
                $allowChanging = 1;
            } 
        }

        if($allowChanging)
        {
            $answer = $command->getAnswer($commandName);
            if($this->vk->sendAnswer($answer))
            {
                $this->command = $commandName;
                return true;
            }
        }
    }


    protected function getCommandInstance($commandName)
    {
        return $instance;
    }

    
    /**
     * isNotRepeat
     * Сравниваем id сообщения со всеми, на которые мы ответили ранее. 
     * @param  mixed $message_id
     *
     * @return boolean
     */
    protected function isNotRepeat($message_id)
    {
        foreach($this->messages_ids as $msg_id)
            if($msg_id == $message_id)
             return 0;

        return 1;
    }

    /**
     * getAnswer
     *
     * @param  mixed $commandName
     *
     * @return array[
     *     text, buttons, attachment
     * ]
     * 
     */
    protected function getAnswer($command)
    {
        $result = [];

        if($this->mode == 'text') //добавить проверку if($command->hideVirtualKeyboard) $result['keyboard'] = 'empty keyboards json';
            $command->text .= '<br>' . $this->generateKeyboard($command,'text');
        else
            $result['buttons'] = $this->generateKeyboard($command,'virtual');

        $result['text'] = $this->matchMessage($command->text);
        $result['attachment'] = $command->attachment;

        return [
            'text','buttons','attachment'
        ];
    }

    /**
     * matchMessage
     *
     * @param  mixed $message
     * Сообщение для замены переменных на соответствующие переменные
     * @param  mixed $valContainer
     * Ассоциативный массив для замены переменных
     *
     * @return string 
     * Сообщение, с замененными названиями переменных на их значения
     */
    protected function matchMessage($message, $valContainer = [])
    {
        preg_match_all('/{(name)}/',$message,$variables);

        foreach($valContainer as $key => $val)
        {
            
        }
    }

    
    /**
     * generateKeyboard
     * Если $type == virtual, вернет command с готовой для отправки вк виртуальной клавиатурой
     * Если $type == text, вернет command с текстом, в который добавлены варианты ответа сгенерированые из клавиатуры
     * @param  mixed $command
     * @param  mixed $type
     *
     * @return command
     */
    private function generateKeyboard($command, $type = 'virtual')
    {
        if($mode == 'virtual')
        {
            return $buttonsJson;
        }
        else
        {
            return $buttonsString;
        }
    }

    /**
     * ЭТО ДОЛЖНО БЫТЬ В КОНТРОЛЛЕРЕ
     * HandleInput
     *  Находит следующую команду исходя из входных данных. Если задан payload, то берет следующую команду из него,
     * если команщда находится в области видимости.
     * Если payload не задан, проходимся по всем text_payload текущей команды и сравниваем с сообщением.
     * Если совпадений не было, а сообщение равно числу, ищем кнопку с таким же номером
     * @param  mixed $input
     *
     * @return void
     */
    public function handleInput($input)
    {
        $nextCommand = $this->detectNextCommand($input);
        if($nextCommand != false)
        {
            $this->changeCommand($nextCommand);
        }
    }

    protected function detectNextCommand($input)
    {
        if(property_exists($input->object,'payload'))
        {
            $payload = json_decode($input->object->payload,1);

            if(array_key_exists('nextCommand',$payload))
                return $payload['nextCommand'];
        }
        
        $currentCommand = $this->getCommandInstance($this->currentCommand);

        $nextCommand = $currentCommand->findCommandWithTextPayload($input->object->text);
        return $nextCommand ? $nextCommand : false;
    }
}