<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Command extends Model
{
    
    public function findCommandWithTextPayload($message)
    {
    	foreach($this->command->buttons as $button)
            {
                foreach($button->text_payloads as $payload)
                {
                    if(mb_ereg_match($payload,$input->object->text))
                    {
                        $this->nextCommand = $button->command;
                    }
                }
                if($nextCommand == null)
                {
                    if(mb_ereg_match('^\d+$',$input->object->text, $num))
                    {
                        foreach($buttons as $i => $button)
                        {
                            if($i == $num[0])
                            {
                                $this->nextCommand = $button->command;
                                break;
                            }
                        }
                    }
                }
            }
    }

    public function commandInVisability($commandName)
    {

    }

        /**
     * getAnswer
     *
     * @param  mixed $commandName
     *
     * @return array[
     *     text, buttons, attachment
     * ]
     * 
     */
    protected function getAnswer($varsForMessage, $mode = 'virtual')
    {
        $result = [
        	'message' => $this->message,
        	'attachment' => $this->attachment 
        ];

        //генерируем клавиатуру в зависимости от переданного способа
        if($mode == 'text') 
            $result['message'] .= '<br>' . $this->generateKeyboard('text');
        else
            $result['buttons'] = $this->generateKeyboard('virtual');

        //заменяем переменные на соответствующие значения
        $result['message'] = $this->matchMessage($result['message'], $varsForMessage);

        return $result;
    }


    /**
     * matchMessage
     *
     * @param  mixed $message
     * Сообщение для замены переменных на соответствующие переменные
     * @param  mixed $valContainer
     * Ассоциативный массив для замены переменных
     *
     * @return string 
     * Сообщение, с замененными названиями переменных на их значения
     */
    protected function matchMessage($message = $this->text,$valContainer = [])
    {
        preg_match_all('/{(name)}/',$message,$variables);

        foreach($valContainer as $key => $val)
        {
            
        }
    }
    


        /**
     * generateKeyboard
     * Если $type == virtual, вернет command с готовой для отправки вк виртуальной клавиатурой
     * Если $type == text, вернет command с текстом, в который добавлены варианты ответа сгенерированые из клавиатуры
     * @param  mixed $command
     * @param  mixed $type
     *
     * @return command
     */
    private function generateKeyboard($command, $type = 'virtual')
    {
    	//добавить проверку if($command->hideVirtualKeyboard) $result['keyboard'] = 'empty keyboards json';
        if($mode == 'virtual')
        {
            return $buttonsJson;
        }
        else
        {
            return $buttonsString;
        }
    }
}	
