<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class NewCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'new:command {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new vk chat command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = ucfirst($this->argument('name'));

        $template = file_get_contents('./Templates/Command.php');
        $template = str_replace('{CommandName}',$name);
        file_put_contents('../../Http/Commands/' . $name . '.php');
        return ("Command $name generated successfuly");
    }
}
